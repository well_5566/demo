package com.test.service;

import com.test.model.Admin;

public interface AdminService {
	Admin checkLogin(String name,String pwd);
}

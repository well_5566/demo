package com.test.dao;

import com.test.model.Admin;

public interface AdminDao {
	Admin findAdminbyname(String name);
}

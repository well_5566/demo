/**
 *  ClassName: MD5Encoder.java
 *  created on 2010-6-4
 *  Copyrights 2010 qjyong All rights reserved.
 *  site: http://blog.csdn.net/qjyong
 *  email: qjyong@gmail.com
 */
package com.test.util;

import java.security.MessageDigest;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;

/**
 * MD5加密工具
 */
public class MD5Encoder {
	/**
	 * md5生成
	 * @param src
	 * @return
	 * @throws Exception
	 */
	public  static String md5(String src) throws Exception{
		//采用MD5将密码处理
		MessageDigest md = 
			MessageDigest.getInstance("MD5");
		byte[] input = src.getBytes();
		byte[] output = md.digest(input);
		//采用Base64将output转成字符串
		String dest = 
			Base64.encodeBase64String(output);
		return dest;
	}
	public static void main(String[] args) throws Exception {
		String s = md5("123456");
		System.out.println(s);
		UUID uuid = UUID.randomUUID();
		System.out.println(uuid);
	}
}

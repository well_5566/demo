package com.test.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.test.model.Admin;
import com.test.service.AdminService;

@Controller
@RequestMapping("/Admin")
public class AdminController {
	@Autowired
	private AdminService adminService;
	/**
	 * 查找用户
	 * @param name
	 * @param pwd
	 * @param request
	 * @return
	 */
	@RequestMapping("/findAdmin")
	public  String checkLogin(String name,String pwd,HttpServletRequest request,HttpServletResponse response){
		try {
			System.out.println("前台传入的名字是："+name);
			System.out.println("前台传入的密码是："+pwd);
			Admin admin = adminService.checkLogin(name, pwd);
			request.getSession().setAttribute("admin", admin);
			return "/back/admin/index";
		} catch (RuntimeException e) {
			request.setAttribute("error", e.getMessage());
			return "/back/admin/login";
		} catch (Exception e){
			request.setAttribute("error", "暂时无法登录，请稍后再试！");
			return "/back/admin/login";
		}
		
	}
}
